$(document).ready(function(){
	window.map=L.map('map').setView([26, -80.22], 11);
	var control = new L.Control.BingGeocoder('AlAY-bnKDAZKs2G2IovGDZmM6RjLaT9eULsU0DhHd2pjh00rukW9otNQlqdmO1Pl');
	 map.addControl(control);

	L.tileLayer('http://{s}.tile.stamen.com/terrain/{z}/{x}/{y}.png', {
	    subdomains: ["d"]
	}).addTo(map);

	var layerGroup = L.layerGroup().addTo(map);

	$.getJSON(
		"twofeet.geojson", function(data){
			var gjson = L.geoJson(data);
			layerGroup.addLayer(gjson);
		}
	);

	$("#dropdown select").change(function(e){
	var filename=$("#dropdown select").val();
	$.getJSON(
			filename + ".geojson", function(data){
				layerGroup.clearLayers();
				var gjson = L.geoJson(data);
				layerGroup.addLayer(gjson);
			}
		)
	});



})

